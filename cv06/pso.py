from collections import namedtuple
from functools import reduce

import numpy as np

import benchmarks


class PSO:
    def __init__(self, fn, dim, bounds, c, w=None, v_max=None, out_of_bounds_penalty=1e6):
        self.fn = fn
        self.dim = dim
        self.bounds = bounds
        self.c = c
        self.w = w
        self.v_max = v_max
        self.out_of_bounds_penalty = out_of_bounds_penalty

    def _fn(self, *xs):
        out_of_bounds = [
            x
            for x, lo, hi in zip(xs, self.bounds[0], self.bounds[1])
            if not lo <= x <= hi
        ]
        return self.fn(*xs) + len(out_of_bounds) * self.out_of_bounds_penalty

    def _best(self, collection):
        return reduce(
            lambda a, b: a if self._fn(*a) < self._fn(*b) else b,
            (p.personal_best if isinstance(p, self.Particle) else p for p in collection)
        )

    class Particle:
        def __init__(self, position, velocity=None, personal_best=None):
            self.position = position
            self.velocity = velocity if velocity is not None else np.zeros_like(position)
            self.personal_best = personal_best if personal_best is not None else position

        def __str__(self):
            return str((self.position, self.velocity, self.personal_best))

    def _random_particle(self):
        return self.Particle(np.array([
            np.random.uniform(lo, hi)
            for _, lo, hi in zip(range(self.dim), self.bounds[0], self.bounds[1])
        ]))

    def _migrate(self, particle, global_best, i, migrations):
        w = self.w[0] - (self.w[0] - self.w[1]) * i / migrations if self.w is not None else 1

        velocity = particle.velocity * w \
                   + self.c[0] * np.random.rand() * (particle.personal_best - particle.position) \
                   + self.c[1] * np.random.rand() * (global_best - particle.position)

        if self.v_max is not None:
            v_norm = np.linalg.norm(velocity)
            factor = self.v_max / v_norm if v_norm > self.v_max else 1
            velocity *= factor

        position = particle.position + velocity
        return self.Particle(position, velocity, self._best((particle.personal_best, position)))

    Step = namedtuple('StepPSO', 'global_best, particles')

    def run(self, population, migrations):
        particles = [self._random_particle() for _ in range(population)]
        global_best = self._best(particles)
        yield self.Step(global_best, particles)

        for i in range(migrations):
            particles = [self._migrate(p, global_best, i, migrations) for p in particles]
            global_best = self._best(particles)
            yield self.Step(global_best, particles)


if __name__ == '__main__':
    f = benchmarks.schwefel
    dm = 2
    b = ((-500,) * dm, (+500,) * dm)
    pso = PSO(
        fn=f,
        dim=dm,
        bounds=b,
        c=(.8, .5),
        w=(.9, .89),
        v_max=10,
    )
    migs = list(pso.run(population=50, migrations=1000))

    last = migs[len(migs) - 1]
    print(last[0])
    for p in last[1]:
        print(p)

    def animate():
        import matplotlib.pyplot as plt
        from mpl_toolkits import mplot3d
        from matplotlib.animation import FuncAnimation

        fig = plt.figure(figsize=(5, 8))

        x, y = np.meshgrid(*[
            np.linspace(xs, ys)
            for xs, ys in zip(b[0], b[1])
        ])
        z = f(x, y)

        # 3D plot

        ax = fig.add_subplot(211, projection='3d')
        ax.plot_surface(x, y, z, cmap='jet')

        ax.set_title('Plot 3D')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')

        # Heat map scatter

        ax = fig.add_subplot(212)
        ax.set_title('Progress animation on heat map')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_xlim(b[0][0], b[1][0])
        ax.set_ylim(b[0][1], b[1][1])

        ax.pcolor(x, y, z, cmap='jet', alpha=.7)

        particle_scatter = ax.scatter([], [], cmap='rainbow', edgecolor='k')
        g_best_scatter = ax.scatter([], [], cmap='xkcd:black', marker='x')

        def update(frame):
            gb, parts = frame[0], frame[1]
            particle_scatter.set_offsets([p.position for p in parts])
            particle_scatter.set_array(np.arange(len(parts)))
            g_best_scatter.set_offsets([gb])
            return particle_scatter, g_best_scatter

        FuncAnimation(fig, update, blit=True, interval=1, frames=migs)

        plt.show()


    animate()
