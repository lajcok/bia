import numpy as np

import benchmarks
from cv08.animate import animate2d


class FireflyAlgorithm:
    def __init__(self, fn, dim, bounds, out_of_bounds_penalty=-1e6):
        self.fn = fn
        self.dim = dim
        self.bounds = tuple(map(np.array, bounds))
        self.out_of_bounds_penalty = out_of_bounds_penalty

    def _normalize(self, x):
        return (x - self.bounds[0]) % (self.bounds[1] - self.bounds[0] + 1) + self.bounds[0]

    def run(self, pop_size, iterations, alpha, omega=1.):
        population = np.random.uniform(low=self.bounds[0], high=self.bounds[1], size=(pop_size, self.dim))
        fitness = np.array([self.fn(*population[i]) for i in range(len(population))])
        for _ in range(iterations):
            for i in range(len(population)):
                for j in range(len(population)):
                    if fitness[j] > fitness[i]:
                        diff = population[j] - population[i]
                        dist = np.linalg.norm(diff)
                        attract = (fitness[j] - fitness[i]) / (omega + dist)
                        population[i] += attract * diff + alpha * np.random.uniform(-1, +1, self.dim)
                        population[i] = self._normalize(population[i])
                fitness[i] = self.fn(*population[i])
            best_i = np.argmax(fitness)
            yield population[best_i].copy(), population.copy()


if __name__ == '__main__':
    _fn = benchmarks.schwefel
    _dim = 2
    _bounds = ((-500,) * _dim, (+500,) * _dim)
    _fa = FireflyAlgorithm(_fn, _dim, _bounds)
    _progress = list(_fa.run(pop_size=10, iterations=10 ** 2, alpha=1.))
    animate2d(_fn, _bounds, _progress)
