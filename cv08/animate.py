import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from matplotlib.animation import FuncAnimation
import numpy as np


def animate2d(fn, bounds, progress):
    fig = plt.figure(figsize=(5, 8))

    x, y = np.meshgrid(*[
        np.linspace(xs, ys)
        for xs, ys in zip(bounds[0], bounds[1])
    ])
    z = fn(x, y)

    # 3D plot

    ax = fig.add_subplot(211, projection='3d')
    ax.plot_surface(x, y, z, cmap='jet')

    ax.set_title('Plot 3D')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    # Heat map scatter

    ax = fig.add_subplot(212)
    ax.set_title('Progress animation on heat map')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_xlim(bounds[0][0], bounds[1][0])
    ax.set_ylim(bounds[0][1], bounds[1][1])

    ax.pcolor(x, y, z, cmap='jet', alpha=.7)

    particle_scatter = ax.scatter([], [], cmap='rainbow', edgecolor='k')
    g_best_scatter = ax.scatter([], [], cmap='xkcd:black', marker='x')

    def update(frame):
        best, individuals = frame[0], frame[1]
        particle_scatter.set_offsets(individuals)
        particle_scatter.set_array(np.arange(len(individuals)))
        g_best_scatter.set_offsets([best])
        return particle_scatter, g_best_scatter

    FuncAnimation(fig, update, blit=True, interval=1, frames=progress)

    plt.show()
