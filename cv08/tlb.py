from functools import reduce

import numpy as np

import benchmarks
from cv08.animate import animate2d


class TLB:
    def __init__(self, fn, dim, bounds, out_of_bounds_penalty=np.inf):
        self.fn = fn
        self.dim = dim
        self.bounds = bounds
        self.out_of_bounds_penalty = out_of_bounds_penalty

    # TODO Wild fireflies flee!
    def _fn(self, *xs):
        out_of_bounds = [
            x
            for x, lo, hi in zip(xs, self.bounds[0], self.bounds[1])
            if not lo <= x <= hi
        ]
        return self.fn(*xs) + (self.out_of_bounds_penalty if len(out_of_bounds) else 0)

    def _random_individual(self):
        return np.random.uniform(self.bounds[0], self.bounds[1], self.dim)

    def _best(self, individuals):
        values = [self._fn(*individual) for individual in individuals]
        best_i = np.argmin(values)
        return individuals[best_i]

    def _teach(self, individual, teacher, mean):
        tf = np.random.randint(1, 3)
        return individual + np.random.uniform(size=self.dim) * (teacher - tf * mean)

    def _learn(self, individual, another):
        better, worse = (individual, another) if self._fn(*individual) <= self._fn(*another) else (another, individual)
        return individual + np.random.uniform(size=self.dim) * (better - worse)

    def run(self, population_size, migrations, teach_rate):
        generation = [self._random_individual() for _ in range(population_size)]
        teacher = self._best(generation)
        mean = np.mean(generation, axis=0)

        def teaching():
            return [
                self._best((individual, self._teach(individual, teacher, mean)))
                for individual in generation
            ]

        def learning():
            return [
                self._learn(individual, generation[np.random.randint(len(generation))])
                for individual in generation
            ]

        for i in range(migrations):
            generation = teaching() if not i % teach_rate else learning()
            yield self._best(generation), generation


if __name__ == '__main__':
    _fn = benchmarks.schwefel
    _dim = 2
    _bounds = ((-500,) * _dim, (+500,) * _dim)
    _tlb = TLB(
        fn=_fn,
        dim=_dim,
        bounds=_bounds,
    )

    _progress = _tlb.run(population_size=10, migrations=10**4, teach_rate=100)
    animate2d(_fn, _bounds, _progress)

