from functools import reduce
from multiprocessing.pool import ThreadPool
from time import time

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from numpy.random.mtrand import normal

import benchmarks

from mpl_toolkits import mplot3d
mplot3d = mplot3d


def scatter_points(center, n, scale):
    return (
        normal(center, scale)
        for _ in range(n)
    )


def filter_points(points, begin, end):
    return filter(
        lambda point: reduce(
            lambda x_fits, y_fits: x_fits and y_fits,
            [
                fr <= pnt <= to
                for fr, to, pnt in zip(begin, end, point)
            ]
        ), points
    )


def find_best(fn, points):
    return tuple(reduce(
        lambda result, x: x if fn(*x) < fn(*result) else result,
        points
    ))


# # Functional, recursive approach
# def hill_climbing(fn, init, points_per_step, steps):
#     step_points = scatter_points(init, points_per_step)
#     current_best = hill_climbing_step(fn, step_points)
#     yield current_best
#     yield from hill_climbing(
#         fn,
#         init=current_best,
#         steps=steps - 1,
#         points_per_step=points_per_step,
#     ) if steps > 1 else []


def hill_climbing(fn, init, points_per_step, scale, steps, begin, end):
    current_best = init
    for _ in range(steps):
        step_points = list(filter_points(
            scatter_points(current_best, points_per_step, scale),
            begin, end
        ))
        current_best = find_best(fn, [*step_points, current_best])
        yield (current_best, step_points)


def run_parallel(n_threads, fn, init, points_per_step, scale, steps, begin, end):
    pool = ThreadPool(processes=n_threads)

    def run_climbing(strt, *args):
        best = strt
        for step in hill_climbing(*args):
            best = step[0]
        return best

    threads = [
        pool.apply_async(run_climbing, (init, fn, init, points_per_step, scale, steps, begin, end))
        for _ in range(n_threads)
    ]

    results = [
        t.get()
        for t in threads
    ]

    return find_best(fn, results)


def main():
    f = benchmarks.schwefel

    begin = (-500, -500)
    end = (+500, +500)

    fig = plt.figure(figsize=(10, 20))

    x, y = np.meshgrid(*[
        np.linspace(xs, ys)
        for xs, ys in zip(begin, end)
    ])
    z = f(x, y)

    # 3D plot

    ax = fig.add_subplot(211, projection='3d')
    ax.plot_surface(x, y, z, cmap='winter')

    ax.set_title('Plot 3D')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    # Heat map scatter

    ax = fig.add_subplot(212)

    ax.pcolor(x, y, z, cmap='winter')

    tstart = time()
    init = best = (0, 0)
    hc = list(hill_climbing(
        fn=f,
        init=best,
        points_per_step=100,
        scale=100,
        steps=300,
        begin=begin,
        end=end,
    ))
    tend = time()

    colors = cm.spring(np.linspace(0, 1, len(hc)))
    for (step, cloud), c in zip(hc, colors):
        best = step
        ax.scatter(
            x=[x for x, y in cloud],
            y=[y for x, y in cloud],
            color=c,
            alpha=.1,
            marker='.',
        )
        ax.scatter(
            *step,
            color=c,
            marker='x',
        )

    print(f'Hill Climbing minimum found: f{best} = {f(*best)}')
    print(f'Sequential took {tend - tstart} s')

    # tstart = time()
    # parallel = run_parallel(
    #     n_threads=8,
    #     fn=f,
    #     init=init,
    #     points_per_step=ceil(100/8),
    #     scale=100,
    #     steps=300,
    #     begin=begin,
    #     end=end,
    # )
    # tend = time()
    # print(f'Hill Climbing minimum found: f{parallel} = {f(*parallel)}')
    # print(f'Parallel took {tend - tstart} s')

    plt.show()


if __name__ == '__main__':
    main()
