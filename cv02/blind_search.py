import random
from math import ceil
from multiprocessing.pool import ThreadPool
from time import time

import matplotlib.pyplot as plt
from functools import reduce

import numpy as np

import benchmarks

from mpl_toolkits import mplot3d

mplot3d = mplot3d


def scatter_points(begin, end, n):
    return (
        tuple([
            random.uniform(b, e)
            for b, e in zip(begin, end)
        ])
        for _ in range(n)
    )


def blind_search_naive(points, fn=None):
    return reduce(
        lambda result, x: x,
        points
    )


def blind_search_memory(points, fn):
    return reduce(
        lambda result, x: x if fn(*x) < fn(*result) else result,
        points
    )


def run_parallel(points, n_threads, fn=None, method=blind_search_memory):
    chunk = ceil(len(points) / n_threads)
    pool = ThreadPool(processes=n_threads)

    threads = [
        pool.apply_async(method, (points[i * chunk:(i + 1) * chunk], fn))
        for i in range(n_threads)
    ]

    results = [
        t.get()
        for t in threads
    ]

    return method(results, fn)


def main(graphics=False):
    f = benchmarks.schwefel

    begin = (-500, -500)
    end = (+500, +500)

    points = list(scatter_points(begin, end, n=5000))

    naive = blind_search_naive(points)
    print(f'Blind search naive: f{naive} = {f(*naive)}')

    start = time()
    memory = blind_search_memory(points, f)
    print(f'Blind search w/ memory: f{memory} = {f(*memory)}; took {time() - start} s')

    # start = time()
    # parallel = run_parallel(points, 10, f)
    # print(f'Blind search parallel: f{parallel} = {f(*parallel)}; took {time() - start} s')

    if graphics:
        fig = plt.figure(figsize=(10, 20))

        x, y = np.meshgrid(*[
            np.linspace(xs, ys)
            for xs, ys in zip(begin, end)
        ])
        z = f(x, y)

        # 3D plot
        ax = fig.add_subplot(211, projection='3d')
        ax.plot_surface(x, y, z, cmap='winter')

        ax.set_title('Plot 3D')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')

        # Heat map scatter

        ax = fig.add_subplot(212)
        ax.pcolor(x, y, z, cmap='winter')
        ax.scatter(
            x=[x for x, y in points],
            y=[y for x, y in points],
            c='xkcd:black',
            alpha=.2,
            marker='.',
        )

        ax.scatter(*naive, c='xkcd:yellowgreen')
        ax.scatter(*memory, c='xkcd:orange')

        ax.set_title('Blind Search scatter w/ heat map')
        ax.set_xlabel('x')
        ax.set_ylabel('y')

        plt.show()


if __name__ == '__main__':
    main(True)
