from functools import reduce

import numpy as np

import benchmarks
from cv08.animate import animate2d


class DifferentialEvolution:
    def __init__(self, fn, dim, bounds, out_of_bounds_penalty=1e6):
        self.fn = fn
        self.dim = dim
        self.bounds = bounds
        self.out_of_bounds_penalty = out_of_bounds_penalty

    def _fn(self, *xs):
        out_of_bounds = [
            x
            for x, lo, hi in zip(xs, self.bounds[0], self.bounds[1])
            if not lo <= x <= hi
        ]
        return self.fn(*xs) + len(out_of_bounds) * self.out_of_bounds_penalty

    def _best(self, population):
        return reduce(
            lambda a, b: a if self._fn(*a) <= self._fn(*b) else b,
            population
        )

    def mutate_rand(self, scaling_factor=.5, level=1):
        def _mutate(ind, pop):
            rs = np.random.choice(
                np.arange(pop.shape[0]),
                size=2 * level + 1,
                replace=True,
            )
            r1 = pop[rs[0]]
            rn = [
                pop[rs[i]] - pop[rs[i + 1]]
                for i in range(1, len(r1), 2)
            ]
            return r1 + scaling_factor * sum(rn)

        return _mutate

    @staticmethod
    def crossover(original, mutant, crossover_rate):
        return np.asarray([
            m if np.random.rand() <= crossover_rate or np.random.rand() < 1 / len(original) else o
            for o, m in zip(original, mutant)
        ])

    def run(self, population_size, generations, mutate=None, crossover_rate=.95):
        mutate = mutate or self.mutate_rand()
        population = np.random.uniform(
            low=self.bounds[0],
            high=self.bounds[1],
            size=(population_size, self.dim)
        )
        yield self._best(population), population

        for _ in range(generations):
            mutants = [
                self.crossover(individual, mutate(individual, population), crossover_rate)
                for individual in population
            ]
            for i, mutant in enumerate(mutants):
                if self._fn(*mutant) <= self._fn(*population[i]):
                    population[i] = mutant
            yield self._best(population), population


if __name__ == '__main__':
    _fn = benchmarks.schwefel
    _dim = 2
    _bounds = ((-500,) * _dim, (+500,) * _dim)
    _de = DifferentialEvolution(_fn, _dim, _bounds)
    _progress = [
        (_bi, _pop.copy())
        for _bi, _pop in _de.run(population_size=20, generations=100)
    ]
    animate2d(_fn, _bounds, _progress)
