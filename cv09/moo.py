import numpy as np

import benchmarks


# just the sorting on functions from randomly generated set of inputs


class Pareto:
    def __init__(self, fns):
        self.fns = fns

    def crowding(self, xs):
        def _crowd_dist(fn):
            fn_eval = [fn(*x) for x in xs]
            fn_ord_i = sorted(range(len(xs)), key=lambda i: fn_eval[i])
            fn_diff = np.max(fn_eval) - np.min(fn_eval)
            dists = {
                ix: fn_eval[fn_ord_i[i + 1]] - fn_eval[fn_ord_i[i - 1]] / fn_diff
                    if 0 < i < len(fn_ord_i) - 1 else np.inf
                for i, ix in enumerate(fn_ord_i)
            }
            return np.asarray([dists[i] for i, _ in enumerate(xs)])

        crowd_dists = sum(map(_crowd_dist, self.fns))
        ord_ixs = sorted(range(len(xs)), key=lambda i: crowd_dists[i], reverse=True)
        return [ix for ix in ord_ixs]

    def dominates(self, p, q):
        return not [
            None
            for fn in self.fns
            if not fn(*p) < fn(*q)
        ]

    def ranks(self, xs):
        Fi = set()
        S = [set() for _ in xs]
        n = np.zeros(len(xs), dtype=int)
        for i, p in enumerate(xs):
            for j, q in enumerate(xs):
                if self.dominates(p, q):
                    S[i].add(j)
                elif self.dominates(q, p):
                    n[i] += 1
            if n[i] == 0:
                Fi.add(i)
        while Fi:
            yield Fi
            Q = set()
            for i in Fi:
                for j in S[i]:
                    n[j] -= 1
                    if n[j] == 0:
                        Q.add(j)
            Fi = Q


def visualize(fns, xs):
    import matplotlib.pyplot as plt
    x, y = tuple([fn(*x) for x in xs] for fn in fns)
    plt.scatter(x, y)
    for i, xy in enumerate(zip(x, y)):
        plt.annotate(i, xy)
    plt.show()


if __name__ == '__main__':
    _fns = benchmarks.dejong, benchmarks.schwefel
    _p = Pareto(_fns)
    _xs = [np.random.uniform(-100, +100, 2) for _ in range(50)]
    visualize(_fns, _xs)
    for _i, _rank in enumerate(_p.ranks(_xs)):
        _rank_list = list(_rank)
        _rank_xs = [_xs[_ix] for _ix in _rank_list]
        _crowding_ixs = _p.crowding(_rank_xs)
        print(_i, [_rank_list[_rix] for _rix in _crowding_ixs])
