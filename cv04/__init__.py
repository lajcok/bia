# Dot product
def dot(a, b):
    return sum(map(lambda t: t[0] * t[1], zip(a, b)))


# Euclidean norm
def norm(a):
    return dot(a, a) ** (1 / 2)


# Euclidean distance
def dist(a, b):
    return norm(tuple(map(lambda t: t[0] - t[1], zip(a, b))))


# Compute distance of permutation
def cycle_dist(perm):
    return sum([
        dist(a, b)
        for a, b in zip(perm, perm[1:] + perm[:1])
    ])
