import random
import numpy as np
from cv04 import cycle_dist


def pair(u):
    return [
        (x, y)
        for x, y in zip(u, random.sample(u, len(u)))
    ]


def crossover(x, y):
    size = min(len(x), len(y))
    xpt = random.randrange(1, size)  # implicit crossover rate = 2 / size

    def recombine(a, b):
        start = a[:xpt]
        end = [
            bi
            for bi in b
            if bi not in start
        ]
        return start + end

    return recombine(x, y), recombine(y, x)


def mutation(x):
    def rnd():
        return random.randrange(0, len(x))

    a, b = rnd(), rnd()
    xm = list(x)

    tmp = xm[a]
    xm[a] = xm[b]
    xm[b] = tmp
    return xm


def selection(u, k, f):
    sorted_u = sorted(list(u), key=f)
    weights = [
        2 ** -i
        for i in range(1, len(sorted_u) + 1)
    ]
    prob = np.array(weights) / np.sum(weights)
    indices = np.random.choice(
        len(sorted_u),
        size=k,
        replace=False,
        p=prob,
    )
    return [sorted_u[i] for i in indices]


def genetic_tsp(u, f, it, cr, mr, fv=None,
                x_over=lambda x, y: crossover(*crossover(x, y)),
                mutate=mutation,
                parents_per_gen=4):
    if len(u) < 1:
        raise ValueError('Expected un-empty list')

    for _ in range(it):
        sel = selection(u, len(u), f)

        parents = sel[:parents_per_gen]
        offspring = [
            mutate(e) if random.random() < mr else e
            for x, y in pair(parents)
            for e in (x_over(x, y) if random.random() < cr else (x, y))
        ]

        lbd = [
            o if f(o) < f(p) else p
            for (o, p) in zip(offspring, parents)
        ]
        u = lbd + sel[parents_per_gen:]

        gen = [(x, f(x)) for x in u]
        yield gen

        if fv is not None:
            for g in gen:
                if g[1] <= fv:
                    return g


if __name__ == '__main__':
    def best_of_best(gen):
        the_best = None
        for e in gen:
            if not the_best or e[1] < the_best[1]:
                the_best = e
        return the_best


    def visualize(solution):
        import matplotlib.pyplot as plt

        x, y = [node[0] for node in solution], [node[1] for node in solution]

        plt.plot(x + x[:1], y + y[:1])
        plt.scatter(x, y, c='xkcd:red')

        for i, node in enumerate(solution):
            plt.annotate(i, node)

        plt.show()


    import os
    import pandas as ps

    file = os.path.join(os.path.dirname(__file__), 'a280.tsp')
    data = ps.read_csv(
        file,
        skiprows=6,
        nrows=280,  # Take first N rows
        sep=r'\s+',
        names=('n', 'x', 'y'),
    )
    nodes = list(zip(data.x, data.y))

    print(f'Nodes: {nodes}')

    initial = [random.sample(nodes, len(nodes)) for _ in range(50)]
    best = (None, None)

    g_tsp = genetic_tsp(
        u=initial,
        f=cycle_dist,
        it=10 ** 3,
        cr=.5,
        mr=.5,
        fv=5e3,
        x_over=crossover,
    )
    for generation in g_tsp:
        best = best_of_best(generation)
        # print(best)

    print(f'\nFinal: {best[0]}\nLength: {best[1]}')
    visualize(best[0])
