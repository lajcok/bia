from collections import namedtuple

import numpy as np
from numpy.ma import exp
from numpy.random.mtrand import rand

from benchmarks import schwefel


def random_neighbor(x0, M, offset=1):
    neighbor = np.array(x0) + offset * (rand(len(x0)) * 2 - 1)
    return np.array([
        max(x, mn) if x < mn else
        min(x, mx) if x > mx else x
        for x, mn, mx in zip(neighbor, M[0], M[1])
    ])


def neighbors(x0=None, M=None, offset=None):
    return lambda **kwargs: (
        random_neighbor(
            kwargs['x0'] if 'x0' in kwargs else x0,
            kwargs['M'] if 'M' in kwargs else M,
            kwargs['offset'] if 'offset' in kwargs else offset,
        )
        for _ in iter(int, 1)
    )


def simulated_annealing(M, x0, f, T0, Tf, a, nT, N=neighbors(offset=100)):
    Step = namedtuple('StepSA', 'x0, fx0, T')
    T = T0
    while not T < Tf:
        for _ in range(nT):
            yield Step(x0, f(*x0), T)
            x = next(N(x0=x0, M=M))
            df = f(*x) - f(*x0)
            if df < 0 or rand() < exp(-abs(df) / T):
                x0 = x
        T = a(T)


def geometric_reduction(decrement=.99):
    return lambda T: decrement * T


def gradual_reduction(beta=1e-5):
    return lambda T: T / (1 + beta * T)


if __name__ == '__main__':
    bounds = ((-500, -500), (500, 500))
    fn = schwefel

    steps = list(simulated_annealing(
        M=bounds,
        x0=(0, 0),
        f=fn,
        T0=10 ** 3,
        Tf=10,
        a=geometric_reduction(),
        nT=20,
        # a=gradual_reduction(),
        # nT=1,
        N=neighbors(offset=70),
    ))

    for step in steps:
        print(f'x0={tuple(step.x0)};\tf(x0)={step.fx0};\tT={step.T}')
    print(f'Steps made: {len(steps)}')


    def visualize():
        import matplotlib.pyplot as plt
        from mpl_toolkits import mplot3d

        fig = plt.figure(figsize=(10, 20))

        x, y = np.meshgrid(*[
            np.linspace(xs, ys)
            for xs, ys in zip(bounds[0], bounds[1])
        ])
        z = fn(x, y)

        # 3D plot

        ax = fig.add_subplot(211, projection='3d')
        ax.plot_surface(x, y, z, cmap='winter')

        ax.set_title('Plot 3D')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')

        # Heat map scatter

        ax = fig.add_subplot(212)

        ax.pcolor(x, y, z, cmap='winter')

        ax.scatter(
            x=[step.x0[0] for step in steps],
            y=[step.x0[1] for step in steps],
            c=range(len(steps)),
            cmap='spring',
            marker='.',
            alpha=.4,
        )

        ax.scatter(
            x=steps[len(steps) - 1].x0[0],
            y=steps[len(steps) - 1].x0[1],
            color='xkcd:black',
            marker='x',
        )

        plt.show()


    visualize()
