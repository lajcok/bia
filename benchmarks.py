from numpy import sin, sqrt, fabs


def dejong(*xs):
    return sum(x ** 2 for x in xs)


def schwefel(*xs):
    return sum(
        -x * sin(sqrt(fabs(x)))
        for x in xs
    )
