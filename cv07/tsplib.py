import numpy as np
import pandas as ps


class TSPLib:
    def __init__(self, path, head_len, foot_len=1, limit=None):
        self.data = ps.read_csv(
            path,
            skiprows=head_len,
            skipfooter=foot_len,
            nrows=limit,
            delim_whitespace=True,
            names=('n', 'x', 'y'),
            index_col='n',
            skipinitialspace=True,
            engine='python',
        )

        self.nodes = list(map(np.array, zip(self.data.x, self.data.y)))
        self.dist_matrix = np.zeros(shape=(len(self.nodes),) * 2)

        for i in range(len(self.nodes)):
            for j in range(i):
                self.dist_matrix[i, j] = self.dist_matrix[j, i] = np.linalg.norm(self.nodes[i] - self.nodes[j])

    def dist(self, a, b):
        return self.dist_matrix[a, b]

    def cycle_dist(self, perm):
        return sum([
            self.dist_matrix[a, b]
            for a, b in zip(perm, perm[1:] + perm[:1])
        ])

    def id_of(self, node_i):
        return node_i + 1 if self.data.first_valid_index() <= node_i + 1 <= self.data.last_valid_index() else None

    def visualize2d(self, solution):
        import matplotlib.pyplot as plt

        node_solution = [self.nodes[node_i] for node_i in solution]
        x, y = ([node[dim] for node in node_solution] for dim in range(2))

        plt.plot(x + x[:1], y + y[:1], c='b')
        plt.scatter(x, y, c='r')

        for node_i, node in enumerate(node_solution):
            plt.annotate(self.id_of(node_i), node + .5, color='grey', fontsize='x-small')

        plt.show()


if __name__ == '__main__':
    tsp = TSPLib('xqf131.tsp', head_len=8)
