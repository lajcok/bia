import sys
import numpy as np

from cv07.tsplib import TSPLib


class ACO:
    def __init__(self, nodes, dist, fn, alpha=1., beta=2., q=1., vaporization=.5, pheromone_limit=None):
        self.nodes = nodes
        self.fn = fn
        self.alpha = alpha
        self.beta = beta
        self.q = q
        self.vaporization = vaporization
        self.pheromone_limit = pheromone_limit
        self.visibility = np.asarray([
            list(map(lambda d: d ** -1 if d > 0 else 0, (
                dist(a, b)
                for b in self.nodes
            )))
            for a in self.nodes
        ])

    class Ant:
        def __init__(self, visibility, perm=None):
            self.perm = perm or []
            self.current = self.perm[len(self.perm) - 1] if len(self.perm) > 0 else None
            self.vision = visibility.copy()
            self.vision[:, self.perm] = 0

        def move(self, next_node):
            if next_node in self.perm:
                raise ValueError(f'Given node {next_node} already visited')
            return self.__class__(self.vision, self.perm + [next_node])

    def choose_next(self, ant, pheromones):
        if ant.current is None:
            return np.random.choice(self.nodes)
        # TODO Filter out already visited (=0)
        weights = pheromones[ant.current] ** self.alpha * ant.vision[ant.current] ** self.beta
        return np.random.choice(self.nodes, p=weights / np.sum(weights))

    def normalize_pheromones(self, pheromones):
        if self.pheromone_limit is not None:
            for i in range(pheromones.shape[0]):
                for j in range(pheromones.shape[1]):
                    pheromones[i, j] = max(self.pheromone_limit[0], min(pheromones[i, j], self.pheromone_limit[1]))

    def run(self, population_size, iterations):
        pheromones = np.ones(shape=(len(self.nodes),) * 2)
        best = None
        for _ in range(iterations):
            ants = [
                self.Ant(self.visibility)
                for _ in range(population_size)
            ]
            for _ in range(len(self.nodes)):
                ants = [
                    ant.move(self.choose_next(ant, pheromones))
                    for ant in ants
                ]

            pheromones *= 1 - self.vaporization
            distances = list(map(self.fn, map(lambda a: a.perm, ants)))

            for ant, dst in zip(ants, distances):
                if best is None or dst < best[1]:
                    best = ant.perm, dst
                # adds pheromone to visited edges
                pheromones[ant.perm, ant.perm[1:] + ant.perm[:1]] += self.q / dst

            self.normalize_pheromones(pheromones)
            yield best, ants, pheromones.copy()


if __name__ == '__main__':
    path = sys.argv[1] if len(sys.argv) > 1 else 'xqf131.tsp'
    head = int(sys.argv[2]) if len(sys.argv) > 2 else 8
    print(f'Processing {path}')

    tsp = TSPLib(path, head_len=head)
    aco = ACO(
        nodes=list(range(len(tsp.nodes))),
        dist=tsp.dist,
        fn=tsp.cycle_dist,
        alpha=1,
        beta=3,
        q=2,
        vaporization=.3,
        # pheromone_limit=(1e-10, 1),
    )
    progress = list(aco.run(population_size=10, iterations=100))

    _the_best = None
    for _best, _ants, _pheromones in progress:
        print('best:', _best[1], _best[0])
        _the_best = _best[0]
        print(np.min(_pheromones), np.max(_pheromones))

    tsp.visualize2d(_the_best)
