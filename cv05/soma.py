from collections import namedtuple
from functools import reduce

import numpy as np

import benchmarks as b
from cv05 import dist


def soma(pop_size, dim, fn, bounds, migrations,
         path_len=2,
         step_len=.11,
         prt=.5,
         min_div=None,
         out_of_bounds_penalty=1e9,
         cluster_limit=None):
    Step = namedtuple('StepSOMA', 'leader, population, steps')

    def _fn(*xs):
        out_of_bounds = [
            x
            for x, l, h in zip(xs, bounds[0], bounds[1])
            if x < l or x > h
        ]
        return fn(*xs) + len(out_of_bounds) * out_of_bounds_penalty

    def _rand_individual():
        return np.array([
            np.random.uniform(l, h)
            for d, l, h in zip(range(dim), bounds[0], bounds[1])
        ])

    def _best(_items):
        return reduce(lambda a, b: a if _fn(*a) < _fn(*b) else b, _items)

    def _migrate(_individual, _leader):
        prt_vector = np.array([
            int(np.random.random() < prt)
            for _ in _individual
        ])
        return [
            _individual + prt_vector * step_n * step_len * (_leader - _individual)
            for step_n in stepper
        ]

    def _diversity(_population):
        _eval = [_fn(*x) for x in _population]
        return abs(max(_eval) - min(_eval))

    def _cluster(_population):
        a, b, dst = reduce(lambda u, v: u if u[2] < v[2] else v, [
            (u, v, dist(tuple(u), tuple(v)))
            for u in _population
            for v in _population
            if (u != v).any()
        ])
        worse = a if _fn(*a) > _fn(*b) else b
        return [
            _rand_individual() if (_ind == worse).all() else _ind
            for _ind in _population
        ] if dst < cluster_limit else _population

    population = [
        _rand_individual()
        for _ in range(pop_size)
    ]
    steps = [[] for _ in range(pop_size)]
    stepper = list(range(1, int(path_len // step_len) + 1))

    for _ in range(migrations):
        leader = _best(population)
        yield Step(leader, population, steps)

        if min_div is not None and _diversity(population) < min_div:
            return leader

        steps = [_migrate(individual, leader) for individual in population]
        population = [_best(step) for step in steps]

        if cluster_limit is not None and _diversity(population) > 100 * min_div:
            population = _cluster(population)


if __name__ == '__main__':
    dm = 2
    fn = b.schwefel
    b = ((-500,) * dm, (+500,) * dm)
    ea = list(soma(
        pop_size=10,
        dim=dm,
        fn=fn,
        bounds=b,
        migrations=10 ** 3,
        path_len=2,
        step_len=.11,
        min_div=5,
        cluster_limit=20,
    ))

    for mig in ea:
        print(mig)


    def animate():
        import matplotlib.pyplot as plt
        from mpl_toolkits import mplot3d
        from matplotlib.animation import FuncAnimation

        fig = plt.figure(figsize=(5, 8))

        x, y = np.meshgrid(*[
            np.linspace(xs, ys)
            for xs, ys in zip(b[0], b[1])
        ])
        z = fn(x, y)

        # 3D plot

        ax = fig.add_subplot(211, projection='3d')
        ax.plot_surface(x, y, z, cmap='jet')

        ax.set_title('Plot 3D')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')

        # Heat map scatter

        ax = fig.add_subplot(212)
        ax.set_title('Progress animation on heat map')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_xlim(b[0][0], b[1][0])
        ax.set_ylim(b[0][1], b[1][1])

        ax.pcolor(x, y, z, cmap='jet', alpha=.7)

        scatter = ax.scatter([], [], cmap='rainbow', edgecolor='k')

        def update(frame):
            mig, i = frame[0], frame[1]
            leader_i = [i for i in range(len(mig.population)) if (mig.population[i] == mig.leader).all()][0]

            pop = [step[i] for step in mig.steps] if i is not None else mig.population
            scatter.set_offsets(pop + [mig.leader])
            scatter.set_sizes(np.array([30] * len(mig.population) + [100]))
            scatter.set_array(np.arange(len(mig.population)) + [leader_i])

            return scatter,

        FuncAnimation(fig, update, blit=True, interval=1, frames=[
            (mig, i)
            for mig in ea
            for i in list(range(len(mig.steps[0]))) + [None]
        ])

        plt.show()


    animate()
