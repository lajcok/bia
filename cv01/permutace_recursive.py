import string
import sys
from time import time

n = int(sys.argv[1])
alphabet = set(string.ascii_uppercase[:n])

print(f'Alphabet ({n}): {alphabet}')


def permutation(input_set):
    input_set = frozenset(input_set)
    return (
        (a,) + b
        for a in input_set
        for b in permutation(input_set - {a})
    ) if input_set else {tuple()}


counter = 0
start = time()

print('Permutation:')
for perm in permutation(alphabet):
    counter += 1
    print(f'{counter} {perm}')

print(f'Generated {counter} permutations in {time() - start} seconds')
