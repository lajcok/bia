import matplotlib.pyplot as plt
import numpy as np

from mpl_toolkits import mplot3d
mplot3d = mplot3d


def func(x, y):
    return np.sin(np.sqrt(x ** 2 + y ** 2))


x = y = np.linspace(-6, +6, 30)

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')

X, Y = np.meshgrid(x, y)
Z = func(X, Y)
ax.plot_surface(X, Y, Z, cmap='winter')

plt.show()
