import string
import sys
from time import time

n = int(sys.argv[1])
alphabet = set(string.ascii_uppercase[:n])

print(f'Alphabet ({n}): {alphabet}')


def permutation(input_set):
    input_set = frozenset(input_set)
    pending = {(x,) for x in input_set} if input_set else {tuple()}
    while pending:
        a = pending.pop()
        missing = input_set - set(a)
        if missing:
            for b in missing:
                pending.add(a + (b,))
        else:
            yield a


counter = 0
start = time()

print('Permutation:')
for perm in permutation(alphabet):
    counter += 1
    print(f'{counter} {perm}')

print(f'Generated {counter} permutations in {time() - start} seconds')
